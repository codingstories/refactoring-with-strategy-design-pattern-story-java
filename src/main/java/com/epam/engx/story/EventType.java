package com.epam.engx.story;

public enum EventType {
  CREATED,UPDATED,DELETED,SOFT_ARCHIVED,HARD_ARCHIVED
}
