package com.epam.engx.story;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * clone of class from real project. Just to explain the refactoring with design pattern in spring boot with smart DI.
 * It has compilation issues due to unavailability of actual dependencies and classes.
 */
@Component
public class ElasticSearchDispatcher {

  private final RetryConfig retryConfig;
  private final ResourceManagerClientProxy resourceManagerClientProxy;
  private final ResourceTemplateRepository resourceTemplateRepository;
  private final Map<EventType, IEventHandler> handlersMap;

  @Autowired
  public ElasticSearchDispatcher(final ResourceManagerClientProxy resourceManagerClientProxy,
                                 final ResourceTemplateRepository resourceTemplateRepository,
                                 final List<IEventHandler> listHandlers) {
    this.resourceManagerClientProxy = resourceManagerClientProxy;
    this.resourceTemplateRepository = resourceTemplateRepository;
    handlersMap = listHandlers.stream().collect(Collectors.toMap(IEventHandler::handleType, Function.identity()));
    this.retryConfig = new RetryConfigBuilder(false)
        .withDelayBetweenTries(Constant.DELAY_BETWEEN_TRIES_MILLIS, ChronoUnit.MILLIS)
        .withFixedBackoff()
        .retryOnAnyException()
        .withMaxNumberOfTries(Constant.MAX_RETRIES)
        .build();
  }

  public void processEvent(SpecificRecordBase recordBase, ResourceTemplate resourceTemplate) {
    final String resourceLink = Objects.nonNull(recordBase.get(RESOURCE_LINK_FIELD_NAME)) ? recordBase.get(RESOURCE_LINK_FIELD_NAME).toString() : EMPTY_STRING;
    final EventType eventType = Objects.nonNull(recordBase.get(EVENT_TYPE_FIELD_NAME)) ? EventType.valueOf(recordBase.get(EVENT_TYPE_FIELD_NAME).toString()) : EventType.ACTION;
    Resource resource;
    if (eventType == EventType.DELETED) {
      resource = getToBeDeletedResource(recordBase, resourceTemplate);
    } else {
      resource = resourceManagerClientProxy.getResourceByLink(resourceLink, Resource.class);
    }
    final IEventHandler handler = handlersMap.get(eventType);
    CustomOptional.ofNullable(handler).ifPresent(h -> h.handle(resource, resourceTemplate.getMetadata().getAdvancedSearchIndexConfig()))
        .orElse(() -> log.info("message=NoHandlerFoundForEventType, eventType={}", eventType));
  }

  @NotNull
  private Resource getToBeDeletedResource(SpecificRecordBase recordBase, ResourceTemplate resourceTemplate) {
    Resource resource;
    final String resourceId = Objects.nonNull(recordBase.get(RESOURCE_ID_FIELD_NAME)) ? recordBase.get(RESOURCE_ID_FIELD_NAME).toString() : EMPTY_STRING;
    resource = new Resource();
    final ResourceMeta resourceMeta = new ResourceMeta();
    resourceMeta.setResourceType(resourceTemplate.getType());
    resourceMeta.setId(resourceId);
    resource.setMetadata(resourceMeta);
    return resource;
  }
}
