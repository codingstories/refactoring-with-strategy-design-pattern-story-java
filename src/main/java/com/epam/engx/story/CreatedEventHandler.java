package com.epam.engx.story;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class CreatedEventHandler implements IEventHandler {

  private final ResourceService esResourceService;

  @Override
  public EventType handlerType() {
    return EventType.CREATED;
  }

  @Override
  public void handle(Resource resource, AdvancedSearchIndexConfig advancedSearchIndexConfig) {
    CustomOptional.ofNullable(advancedSearchIndexConfig.getPrimaryIndex()).ifPresent(p -> esResourceService.createResource(resource, p))
        .orElse(() -> log.info("message=NoPrimaryIndexFoundForResourceWhileCreate, resourceId={}", resource.getMetadata().getId()));

    CustomOptional.ofNullable(advancedSearchIndexConfig.getSecondaryIndex()).ifPresent(s -> esResourceService.createResource(resource, s))
        .orElse(() -> log.info("message=NoSecondaryIndexFoundForResourceWhileCreate, resourceId={}", resource.getMetadata().getId()));
  }
}