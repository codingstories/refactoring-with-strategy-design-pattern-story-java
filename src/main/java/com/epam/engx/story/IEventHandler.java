package com.epam.engx.story;

public interface IEventHandler {

  EventType handlerType();

  void handle(Resource resource, AdvancedSearchIndexConfig advancedSearchIndexConfig);
}
