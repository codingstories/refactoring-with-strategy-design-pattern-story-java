# Refactoring With Right Design Pattern
**To read**: [https://updatemeonpublish.com]

**Estimated reading time**: 5 mins

## Story Outline
This story explains the usage of design patterns while refactoring large methods, methods which can grow with new cases in future.
The example explained is from real time project, it mainly explains the removal of growing IF-ELSEIF or Switch cases in any method using strategy pattern.

## Story Organization
**Story Branch**: main
> `git checkout main`

Tags: #clean_code, #refactoring, #designpatterns
